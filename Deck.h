//
// Created by Chandler on 10/3/2016.
//

#ifndef CARDGAMES_DECK_H
#define CARDGAMES_DECK_H

#include "Card.h"
#include <vector>
#include <string>


using namespace std;

class Deck{
private: //variables
    //value array {A,2,3,4,5,6,7,8,9,10,J,Q,K}
    string values[13] = {"A","2","3","4","5","6","7","8","9","10","J","Q","K"};
    //suit array {C,S,D,H}
    string suits[4] = {"C","S","D","H"};
    //vector<cards> deckVector
    vector <Card> deckVector;


    //functions
    /**
     * for each value(A-K)
     * for each suit
     * create card
     * add card to deck
     * @return
     */
    vector<Card> create();

    /**
     * shuffles deck
     */
    void shuffle();



public:
    //functions
    /**
     * calls create()
     * calls shuffle()
     * @return shuffled deck
     */
    Deck random();

    /**
     * calls create()
     * returns deck in order
     * @return not shuffled deck
     */
    Deck clean();

    /**
     * gets card off top of deck
     * @return card
     */
    Card deal();

    /**
     * prints elements of deck
     * for debugging
     */
    void showDeck();


};


#endif //CARDGAMES_DECK_H
