//
// Created by Chandler on 10/3/2016.
//

#include "Card.h"

using namespace std;

void Card::createCard(string value1, string suit1, int rank1)
{
    suit = suit1;//set given suit(H,S,D,C) to suit
    value = value1;//set given value(A-K) to value
    rank = rank1;//set given rank(1-13) to rank
    return;
}


int Card::getRank(){
    return rank;//returns rank int
}


string Card::getValue(){
    return value;//returns value string
}


string Card::getSuit(){
    return suit;//returns suit char

}

string Card::showCard(){
    string value1 = getValue();//gets value of card
    string suit1 = getSuit();//gets suit of card
    string card;
    card = value1 + suit1;
    return card;
}