//
// Created by Chandler on 9/30/2016.
//

#include "warGame.h"
#include <iostream>

using namespace std;

void warGame::startStuff(){
    cout << "WELCOME TO WAR!!!\n"<< endl;
    cout << instructions << "\n\n" << endl;
    warDeck = warDeck.random();//shuffled deck
    Card card1;
    Card card2;

    //deal 2 cards at a time to player and computer piles
    //up to 52 cards
    for (int deal = 0; deal < 26; deal++ ){
        card1 = warDeck.deal();
        card2 = warDeck.deal();
        player.add(card1);
        computer.add(card2);
    }
}

 void warGame::gameBoard(){
     //print game board
    cout << "==============================" << endl;
    cout << "Computer Pile: " << computer.numOfCards() << endl;
    cout << "Computer Card: [" << computerCard.showCard() << "]" <<endl;
    cout << "                    " << roundWinner << endl;
    cout << "                    War Pile: " << warPile.numOfCards() << endl;
    cout << "Player Card:   [" << playerCard.showCard() << "]" << endl;
    cout << "Player Pile:   " << player.numOfCards() << endl;
    cout << "==============================" << endl;
}

Card warGame::getPlay(Pile playPile){
    //from user or somputer pile
    Card playCard;
    //get top card
    playCard = playPile.deal();
    return playCard;
}

void warGame::compare(Card playerCard, Card computerCard){
    int playerRank;
    int computerRank;
    bool warPileEmpty;//is war pile empty?
    warPileEmpty = warPile.checkEmpty();//check if war pile is empty
    playerRank = playerCard.getRank();//rank of player card
    computerRank = computerCard.getRank();//rank of computer card
    if (playerRank > computerRank){//if player wins
        roundWinner = "Player won round!";
        //add cards to player pile, if war pile isnt empty, add war pile
        player.add(playerCard);//add card to player pile
        player.add(computerCard);//dido
        if (not warPileEmpty){//if there are cards in war pile
            player = player.pileToPile(warPile);//add cards to player pile
            warPile = warPile.empty();
        }
    }
    else if (computerRank > playerRank){
        roundWinner = "Computer won round!";
        //add cards to computer pile, if war pile isnt empty, add war pile
        computer.add(computerCard);//add card to computer pile
        computer.add(playerCard);//dido
        if (not warPileEmpty){//if there are cards in war pile
            computer = computer.pileToPile(warPile);//add cards to computer pile
            warPile = warPile.empty();
        }
    }
    else if (computerRank == playerRank){
        roundWinner = "WAR!!!";
        //if ranks are the same, add cards to war pile
        warPile.add(playerCard);
        warPile.add(computerCard);
    }
    return;
}

void warGame::checkWinner(){
    bool playerEmpty;
    bool computerEmpty;
    playerEmpty = player.checkEmpty();//true if player pile is empty
    computerEmpty = computer.checkEmpty();//true if computer pile is empty
    if (playerEmpty){
        winner = "COMPUTER";
        started = false;//game over
    }
    else if (computerEmpty){
        winner = "PLAYER";
        started = false;//game over
    }
}

void warGame::gameDone(){
    //tell players who won
    cout << endl << "Congratulations!!! " << winner << " WON!!!";
}

void warGame::startGameOfWar() {
    startStuff();
    while (started){
        cout << "\n\n\nPRESS ENTER TO CONTINUE" << endl;//for spacing
        cin.ignore();//user must press enter to continue
        //get card from player
        playerCard = getPlay(player);
        player.removeTop();
        //get card from computer
        computerCard = getPlay(computer);
        computer.removeTop();
        //see who won
        compare(playerCard,computerCard);
        //check if game is over
        checkWinner();
        //print game board
        gameBoard();
        //end game if over
        if (not started){
            continue;
        }
    }
    //show who won :)
    gameDone();
}