//
// Created by Chandler on 10/3/2016.
//

#include "Deck.h"
#include <algorithm>
#include <iostream>

using namespace std;


//Private stuff
vector<Card> Deck::create(){
    //create card
    Card card;
    //for each Index value(A-K)
    for (int valueIndex = 0; valueIndex < 13; ++valueIndex) {
        //for eash suit (C,H,S,D)
        for (int suitIndex = 0; suitIndex < 4; ++suitIndex){
            //rank iterator (1-13)
            int rank = valueIndex + 1;
            //assign values
            card.createCard(values[valueIndex],suits[suitIndex],rank);
            //add to deck
            deckVector.push_back(card);
    }
}
     return deckVector;
}

void Deck::shuffle(){
//randomizes deck
    srand(time(NULL));//random seed for random_shuffle function
    random_shuffle(deckVector.begin(), deckVector.end());//shuffle vector
    return;
}


//Public function
Deck Deck::random() {
    //create Deck object
    Deck newDeck;
    //create a deck of cards
    newDeck.create();
    //shuffle deck
    newDeck.shuffle();
    return newDeck;
}

Deck Deck::clean() {
    //create Deck object
    Deck newDeck;
    //create a deck of cards
    newDeck.create();
    return newDeck;
}

Card Deck::deal() {
    //creates a card object
    Card card;
    //same as top of deckVactor
    card = deckVector[0];
    //removes card from deck
    deckVector.erase(deckVector.begin());
    return card;
}

void Deck::showDeck()
{
    Card card;
    for (unsigned int i = 0; i < sizeof(deckVector); i++)
    {
        card = deckVector.at(i);
        cout << "Card: ";
        card.showCard();
        cout << endl;
    }
    return;
}
