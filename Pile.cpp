//
// Created by Chandler on 10/3/2016.
//

#include "Pile.h"
#include <iostream>

using namespace std;

vector<Card> Pile::add (Card card){
    pileVector.push_back(card);//add card to vectorPile
    return pileVector;
}

Pile Pile::pileToPile (Pile added){
    Card card;
    Pile pile;
    pile.pileVector = pileVector;
    //for each card in pile
    for (unsigned int index = 0; index < added.pileVector.size(); index++){
        card = added.pileVector[index];//get card
        pile.pileVector.push_back(card);//add card to desired pile
    }
    return pile;
}

Card Pile::deal(){
    //creates a card object
    Card card;
    //same as top of deckVactor
    card = pileVector[0];
    //removes card from deck
    pileVector.erase(pileVector.begin());
    return card;
}

Pile Pile::empty(){
    Pile pile;
    pile.pileVector = pileVector;
    pile.pileVector.clear();//clear the vector
    return pile; //
}

bool Pile::checkEmpty() {
    bool empty;
    //if bool is empty: TRUE else:FALSE
    empty = pileVector.empty();
    return empty;
}

void Pile::showPile()
{
    Card card;
    for (unsigned int i = 0; i < sizeof(pileVector); i++)
    {
        card = pileVector.at(i);
        cout << "Card: ";
        card.showCard();
        cout << endl;
    }
    return;
}

int Pile::numOfCards(){
    int size;
    size = pileVector.size();//get size of pile
    return size;//return number of cards in pile
}

Pile Pile::removeTop(){
    //gets rid of top card on pile
    Pile pile;
    pile.pileVector = pileVector;//set pile = to pile being dealt with
    pileVector.erase(pileVector.begin());//remove top card
    return pile;//return modified pile
}