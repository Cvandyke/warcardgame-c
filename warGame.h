//
// Created by Chandler on 9/30/2016.
//

#ifndef CARDGAMES_WARGAME_H
#define CARDGAMES_WARGAME_H

#include "warGame.h"
#include "Pile.h"
#include "Deck.h"
#include "Card.h"
#include <cstdlib>
#include <string>


class warGame{
private: //variables
    //started
    bool started = true;

    //winner
    string winner;

    //roudn winner
    string roundWinner;

    //war deck (to deal to player and computer)
    Deck warDeck;

    //player pile
    Pile player;

    //computer pile
    Pile computer;

    //war pile (for war times)
    Pile warPile;

    //play pile (for 2 playing cards)
    Pile playPile;

    //play card for player
    Card playerCard;

    //play card for computer
    Card computerCard;

    //instructions
    string instructions = "A deck of 52 cards is split into two piles.  One pile for the computer and one pile for the player.  \n"
            "During each turn, each player will turn up one card from their pile and compare the rank of those cards. \n"
            "The player with the higher card takes both cards and add them to their pile.  \n"
            "If the cards have the same rank, then its war.  The previous cards go to the war pile and another card \n"
            "is drawn by each player.  Higher ranked card takes all cards in play and in the war deck.  \n"
            "The game ends when one player holds all the cards. Good Luck! :)\n";



//functions
    /**
     * print instructions
     * create random deck
     * deal all cards in deck to playing piles
     */
    void startStuff();

    /**
     * prints visual game board to console
     */
    void gameBoard();

    /**
     * gets card from pile for play
     * @return
     */
    Card getPlay(Pile playPile);

    /**
     *
     * @param playerCard
     * @param computerCard
     * player rank = rank of playerCard
     * computer rank = rank of computer card
     * if player rank higher
     *      add both cards to player pile
     *      if war pile not empty
     *          add war pile cards
     * if computer rank higher
     *      add both cards to computerPile
     *      if war pile not empty
     *          add war pile cards
     * else ranks are same
     *      WAR!
     *      add cards to war pile
     * @return roundWinner
     */
    void compare(Card playerCard, Card computerCard);

    /**
     * if computerPile.checkEmpty(); check if its empty
     *      winner = "Player"
     *      started = false
     * playerPile.checkEmpty(); check if its empty
     *      winner = "Computer"
     *      started = false
     */
    void checkWinner();

    /**
     * prints who won and closing stuff
     */
    void gameDone();





public: //functions
    /**
     * call start stuff function
     * while started:
     *  get permission to continue
     *  call get play for computer and player piles (get play cards for both)
     *  compare cards (call get play)
     *  check winner
     *  if winner
     *      break
     *  else
     *      print game board
     *
     *gameDone
     */
    void startGameOfWar();
};

#endif //CARDGAMES_WARGAME_H
