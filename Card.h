//
// Created by Chandler on 10/3/2016.
//

#ifndef CARDGAMES_CARD_H
#define CARDGAMES_CARD_H


#include <string>
using namespace std;

class Card{
private: //variables
    //suit  (S,C,D,H)
    string suit;
    //rank  (1-13)
    int rank;
    //value (A-K)
    string value;


public: //functions
    /**
     *
     * @param value = value1
     * @param suit = suit1
     * @param rank = rank1
     * @return
     */
    void createCard(string value1, string suit1, int rank1);

    /**
     * get rank of card
     * @return rank
     */
    int getRank();

    /**
     * get value of card
     * @return value
     */
    string getValue();

    /**
     * get suit of card
     * @return suit
     */
    string getSuit();

    /**
     * prints card in format AS or 3D
     * format value + suit
     */
    string showCard();
};



#endif //CARDGAMES_CARD_H
