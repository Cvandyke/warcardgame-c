//
// Created by Chandler on 10/3/2016.
//

#ifndef CARDGAMES_PILE_H
#define CARDGAMES_PILE_H

#include "Card.h"
#include <vector>


class Pile{
public:
    //variables
    //vector to hold cards
    vector<Card> pileVector;

    //functions
    /**
     * add card to pile (pileVector.push_back(card))
     * @return pile
     */
    vector<Card> add (Card card);

    /**
     *
     * @param cardsToAdd
     * Card card;
     * for each card in cardsToAdd
     * card = card being added (cardsToAdd[index])
     * pileVector.push_back(card)
     * @return pile
     */
    Pile pileToPile (Pile added);

    /**
     * pop top card off
     * remove from pile
     * @return card
     */
    Card deal();

    /**
     * epmtys the pile (pileVector.clear())
     * @return pile
     */
    Pile empty();

    /**
     * bool checkEmpty;
     * bool = if vector is empty (pileVector.empty())
     * @return
     */
    bool checkEmpty();

    /**
     * prints elements of pile
     * for debugging
     */
    void showPile();

    /**
     * gets number of cards
     * @return number
     */
    int numOfCards();

    /**
     * erases top card
     * @return pile
     */
    Pile removeTop();
};


#endif //CARDGAMES_PILE_H
